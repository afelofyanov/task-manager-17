package ru.tsc.felofyanov.tm.command.project;

import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectChangeStatusByIndexCommand extends AbstractProjectCommand {
    @Override
    public String getName() {
        return "project-change-status-by-index";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Change project status by index.";
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");

        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;

        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        getServiceLocator().getProjectService().changeTaskStatusByIndex(index, status);
    }
}
