package ru.tsc.felofyanov.tm.command.task;

import ru.tsc.felofyanov.tm.model.Task;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {
    @Override
    public String getName() {
        return "task-show-by-id";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Show task by id.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = getServiceLocator().getTaskService().findOneById(id);
        showTask(task);
    }
}
