package ru.tsc.felofyanov.tm.command.project;

import ru.tsc.felofyanov.tm.model.Project;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {
    @Override
    public String getName() {
        return "project-remove-by-id";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Remove project by id.";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = getServiceLocator().getProjectService().findOneById(id);
        getServiceLocator().getProjectTaskService().removeProjectById(project.getId());
    }
}
