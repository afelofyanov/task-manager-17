package ru.tsc.felofyanov.tm.command.task;

import ru.tsc.felofyanov.tm.util.TerminalUtil;

public final class TaskUpdateByIdCommand extends AbstractTaskCommand {
    @Override
    public String getName() {
        return "task-update-by-id";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Update task by id.";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY ID]");

        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();

        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();

        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        getServiceLocator().getTaskService().updateById(id, name, description);
    }
}
