package ru.tsc.felofyanov.tm.command.system;

import ru.tsc.felofyanov.tm.command.AbstractCommand;

public final class VersionCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "version";
    }

    @Override
    public String getArgument() {
        return "-v";
    }

    @Override
    public String getDescription() {
        return "Show applicant version.";
    }

    @Override
    public void execute() {
        System.out.println("1.17.0");
    }
}
