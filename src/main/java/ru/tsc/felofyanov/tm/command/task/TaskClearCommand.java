package ru.tsc.felofyanov.tm.command.task;

public final class TaskClearCommand extends AbstractTaskCommand {
    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK CLEAR]");
        getServiceLocator().getTaskService().clear();
    }
}
