package ru.tsc.felofyanov.tm.component;

import ru.tsc.felofyanov.tm.api.repository.ICommandRepository;
import ru.tsc.felofyanov.tm.api.repository.IProjectRepository;
import ru.tsc.felofyanov.tm.api.repository.ITaskRepository;
import ru.tsc.felofyanov.tm.api.service.*;
import ru.tsc.felofyanov.tm.command.AbstractCommand;
import ru.tsc.felofyanov.tm.command.project.*;
import ru.tsc.felofyanov.tm.command.system.*;
import ru.tsc.felofyanov.tm.command.task.*;
import ru.tsc.felofyanov.tm.command.taskproject.BindTaskToProjectCommand;
import ru.tsc.felofyanov.tm.command.taskproject.UnbindTaskFromProjectCommand;
import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.felofyanov.tm.exception.system.CommandNotSupportedException;
import ru.tsc.felofyanov.tm.model.Project;
import ru.tsc.felofyanov.tm.model.Task;
import ru.tsc.felofyanov.tm.repository.CommandRepository;
import ru.tsc.felofyanov.tm.repository.ProjectRepository;
import ru.tsc.felofyanov.tm.repository.TaskRepository;
import ru.tsc.felofyanov.tm.service.*;
import ru.tsc.felofyanov.tm.util.DateUtil;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ILoggerService loggerService = new LoggerService();

    {
        registry(new InfoCommand());
        registry(new VersionCommand());
        registry(new HelpCommand());
        registry(new AboutCommand());
        registry(new CloseCommand());
        registry(new ShowCommand());
        registry(new ShowArgumentCommand());
        registry(new TaskListCommand());
        registry(new TaskListByProjectIdCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new ProjectListCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectCompeteByIdCommand());
        registry(new ProjectCompeteByIndexCommand());
        registry(new UnbindTaskFromProjectCommand());
        registry(new BindTaskToProjectCommand());
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK-MANAGER SHUTTING DOWN**");
            }
        });
    }

    public void run(String[] args) {
        if (processArgument(args)) System.exit(0);

        initData();
        initLogger();

        while (true) {
            try {
                System.out.println("ENTER COMMAND");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.err.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initData() {
        taskService.add(new Task("DEMO TASK", Status.IN_PROGRESS, DateUtil.toDate("06.05.2019")));
        taskService.add(new Task("TEST TASK", Status.NOT_STARTED, DateUtil.toDate("03.01.2017")));
        taskService.add(new Task("MEGA TASK", Status.COMPLETED, DateUtil.toDate("08.07.2021")));
        taskService.add(new Task("BEST TASK", Status.IN_PROGRESS, DateUtil.toDate("05.05.2018")));

        projectService.add(new Project("DEMO PROJECT", Status.NOT_STARTED, DateUtil.toDate("05.06.2019")));
        projectService.add(new Project("TEST PROJECT", Status.IN_PROGRESS, DateUtil.toDate("03.01.2017")));
        projectService.add(new Project("MEGA PROJECT", Status.COMPLETED, DateUtil.toDate("08.07.2021")));
        projectService.add(new Project("BEST PROJECT", Status.IN_PROGRESS, DateUtil.toDate("05.05.2018")));
    }

    public void processCommand(final String command) {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    public void processArgument(final String argument) {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    public boolean processArgument(String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }
}
