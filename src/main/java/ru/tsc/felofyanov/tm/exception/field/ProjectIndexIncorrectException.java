package ru.tsc.felofyanov.tm.exception.field;

public final class ProjectIndexIncorrectException extends AbstractFieldException {

    public ProjectIndexIncorrectException() {
        super("Error! Project index is incorrect...");
    }
}
