package ru.tsc.felofyanov.tm.exception.field;

public final class TaskIndexIncorrectException extends AbstractFieldException {

    public TaskIndexIncorrectException() {
        super("Error! Task index incorrect...");
    }
}
