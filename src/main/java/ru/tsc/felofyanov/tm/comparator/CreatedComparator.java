package ru.tsc.felofyanov.tm.comparator;

import ru.tsc.felofyanov.tm.api.model.IHasCreated;

import java.util.Comparator;

public enum CreatedComparator implements Comparator<IHasCreated> {

    INSTANCE;

    @Override
    public int compare(IHasCreated iHasCreated, IHasCreated t1) {
        if (iHasCreated == null || t1 == null) return 0;
        if (iHasCreated.getCreated() == null || t1.getCreated() == null) return 0;
        return iHasCreated.getCreated().compareTo(t1.getCreated());
    }
}
